#include "Layer.h"
#include <ctime>

namespace NeuralNetworkNS
{
	//Constructors
	Layer::Layer(const unsigned int& neuroNumber, const LayerType& type = LayerType::Hidden) :
		neuronsNumber(neuroNumber)
	{
		layerType = type;
		weights = std::vector<std::vector<double>>(neuroNumber);
		oldWeights = std::vector<std::vector<double>>(neuroNumber);

		if (layerType == LayerType::Output)
			neurons = std::vector<Neuron>(neuroNumber);
		else
		{
			neurons = std::vector<Neuron>(neuroNumber + 1); //if not output layer then +1 dummy neuron
			neurons[neuroNumber].SetNeuronType(NeuronType::Dummy);
		}
	}

	Layer::~Layer()
	{
	}

	//operators
	void Layer::operator=(const Layer& layer)
	{
		neuronsNumber = layer.neuronsNumber;
		layerType = layer.layerType;
		neuronsOnPrevLayer = layer.neuronsOnPrevLayer;

		for (size_t i = 0; i < neuronsNumber; i++)
		{
			neurons[i] = layer.neurons[i];
			for (size_t j = 0; j < neuronsOnPrevLayer; j++)
			{
				weights[i][j] = layer.weights[i][j];
				oldWeights[i][j] = layer.oldWeights[i][j];
			}
		}
	}


	void Layer::SetNeuronsNumber(const unsigned int& number)
	{
		neuronsNumber = number;

		//clear previous values of vectors;
		neurons.clear();
		weights.clear();
		oldWeights.clear();

		//resize vectors
		weights.resize(neuronsNumber);
		oldWeights.resize(neuronsNumber);

		if (layerType == LayerType::Output)
		{
			neurons.resize(neuronsNumber);
		}
		else
		{
			neurons.resize(neuronsNumber + 1);
			neurons[neuronsNumber].SetNeuronType(NeuronType::Dummy); //if not output layer then +1 dummy neuron
		}


		for (size_t i = 0; i < neuronsNumber; i++)
		{
			neurons[i].SetNeuronType(layerType);
		}
	}

	void Layer::SetLayerType(const LayerType& type)
	{
		layerType = type;
		for (size_t i = 0; i < neuronsNumber; i++)
		{
			neurons[i].SetNeuronType(type);
		}
	}

	void Layer::SetWeights(const std::vector<std::vector<double>>& someWeights)
	{
		neuronsOnPrevLayer = someWeights[0].size(); //Number of neurons on previos layer(with Dummy)
		for (size_t i = 0; i < neuronsNumber; i++)
		{
			weights[i].resize(neuronsOnPrevLayer);
			oldWeights[i].resize(neuronsOnPrevLayer);
			std::copy(someWeights[i].begin(), someWeights[i].end(), weights[i].begin());
			std::copy(someWeights[i].begin(), someWeights[i].end(), oldWeights[i].begin());
			/*for (size_t j = 0; j < neuronsOnPrevLayer; j++)
			{
			weights[i][j] = oldWeights[i][j] = sameWeights[i][j];
			}*/
		}
	}

	///Randomize values of weights in range [-0.5;0.5]
	///prevLayerNeuroNumber - Number of neurons on previos layer(with Dummy)
	void Layer::SetWeights(const unsigned int& prevLayerNeuroNumber)
	{
		neuronsOnPrevLayer = prevLayerNeuroNumber;
		srand(time(0));
		for (size_t i = 0; i < neuronsNumber; i++)
		{
			weights[i].resize(prevLayerNeuroNumber);
			oldWeights[i].resize(prevLayerNeuroNumber);
			for (size_t j = 0; j < prevLayerNeuroNumber; j++)
			{
				weights[i][j] = -0.5 + (rand() % 100) / 100;
				oldWeights[i][j] = weights[i][j];
			}
		}
	}

	void Layer::ChangeOldWeights()
	{
		for (size_t i = 0; i < neuronsNumber; i++)
		{
			for (size_t j = 0; j < neuronsOnPrevLayer; j++)
			{
				oldWeights[i][j] = weights[i][j];
			}
		}
	}


}