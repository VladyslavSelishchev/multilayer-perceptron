#ifndef _NWTRAINER_
#define _NWTRAINER_

#include "NeuralNetwork.h"
#include <memory>
#include "Sample.h"
using namespace NeuralNetworkNS;

class NetworkTrainer
{
public:
	NetworkTrainer(NeuralNetwork&);
	~NetworkTrainer();

	void SetSample(const Sample&);

	void ForwardPropagation();
	void BackPropagation();

	void SetLearningRate(const double& rate) { learningRate = rate; }
	double GetLearningRate() const { return learningRate; }

private:
	std::unique_ptr<NeuralNetwork> nw;
	Sample sample; //Sample for education;
	double learningRate;
};

#endif /*_NWTRAINER_*/