#ifndef _SAMPLE_
#define _SAMPLE_
#include <vector>
#include <string>

class Sample //samples that give to network
{
public:
	Sample() {}
	Sample(const std::string&, const unsigned int&, const unsigned int&);
	~Sample();

	void operator=(const Sample&);

public:
	std::vector<double> inputValues;
	std::vector<double> outputValues;
};

#endif /*_SAMPLE_*/
