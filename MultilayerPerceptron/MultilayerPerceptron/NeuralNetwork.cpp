#include "NeuralNetwork.h"

namespace NeuralNetworkNS
{
	NeuralNetwork::NeuralNetwork()
	{
		layersNumber = 2;
		layers = std::vector<Layer>(2);
		layers[0].SetLayerType(NeuronType::Input);
		layers[1].SetLayerType(NeuronType::Output);
	}

	NeuralNetwork::NeuralNetwork(const unsigned int& number)
	{
		layersNumber = number;
		if (layersNumber < 2)
		{
			layers = std::vector<Layer>(2);
			layers[0].SetLayerType(NeuronType::Input);
			layers[1].SetLayerType(NeuronType::Output);

		}
		else
		{
			layers = std::vector<Layer>(layersNumber);
			layers[0].SetLayerType(NeuronType::Input);
			layers[layersNumber - 1].SetLayerType(NeuronType::Output);
		}
	}

	NeuralNetwork::NeuralNetwork(const NeuralNetwork& nw)
	{
		layersNumber = nw.layersNumber;
		for (size_t i = 0; i < layersNumber; i++)
		{
			layers[i] = nw.layers[i];
		}
	}

	NeuralNetwork::~NeuralNetwork()
	{
	}


	void NeuralNetwork::SetLayersNumber(const unsigned int& number)
	{
		layersNumber = number;
		if (layersNumber < 2)
		{
			layers = std::vector<Layer>(2);
			layers[0].SetLayerType(NeuronType::Input);
			layers[1].SetLayerType(NeuronType::Output);

		}
		else
		{
			layers = std::vector<Layer>(layersNumber);
			layers[0].SetLayerType(NeuronType::Input);
			layers[layersNumber - 1].SetLayerType(NeuronType::Output);
		}
	}

	Layer& NeuralNetwork::GetLayer(const unsigned int& index)
	{
		assert(index < layersNumber);
		return layers[index];
	}

	void NeuralNetwork::RandomizeAllWeights()
	{
		for (size_t i = 1; i < layersNumber; i++)
		{
			layers[i].SetWeights(layers[i - 1].GetNeuronsNumber() + 1);
		}
	}

	void NeuralNetwork::FillWeightsOnLayer(const std::vector<std::vector<double>>& vect, const unsigned int& layerIndex)
	{
		layers[layerIndex].SetWeights(vect);
	}

	void NeuralNetwork::SetInput(const std::vector<double>& input)
	{
		unsigned int NumberOfNeurons = layers[0].GetNeuronsNumber();
		assert(input.size() == NumberOfNeurons);

		for (size_t i = 0; i < NumberOfNeurons; i++)
		{
			layers[0].neurons[i].SetState(input[i]);
		}
	}

	void NeuralNetwork::ChangeAllOldWeights()
	{
		for (size_t i = 1; i < layersNumber; i++)
		{
			layers[i].ChangeOldWeights();
		}
	}
}