#include "Sample.h"



Sample::Sample(const std::string& str,
	const unsigned int& inNumber,
	const unsigned int& outNumber)
{
	inputValues = std::vector<double>(inNumber);
	outputValues = std::vector<double>(outNumber);

	for (size_t i = 0; i < inNumber; i++)
	{
		if (str[i] == '0')
			inputValues[i] = -1;
		else if (str[i] == '1')
			inputValues[i] = 1;
	}

	for (size_t i = 0; i < outNumber; i++)
	{
		if (str[inNumber + i] == '0')
			outputValues[i] = -1;
		else if (str[inNumber + i] == '1')
			outputValues[i] = 1;
	}
}

Sample::~Sample()
{
}


void Sample::operator=(const Sample& sample)
{
	inputValues.resize(sample.inputValues.size());
	outputValues.resize(sample.outputValues.size());
	for (size_t i = 0; i < inputValues.size(); i++)
	{
		inputValues[i] = sample.inputValues[i];
	}

	for (size_t i = 0; i < outputValues.size(); i++)
	{
		outputValues[i] = sample.outputValues[i];
	}
}