#ifndef _NEURON_
#define _NEURON_

#include "NeuronType.h"

namespace NeuralNetworkNS
{

	class Neuron
	{
	public://public functions
		Neuron();
		Neuron(NeuronType, const double&);
		~Neuron();

		void SetState(const double&); //setter of state;
		double GetState() const { return state; } //getter of state;

		double GetActivation() const; //getter of Activation function
		double GetActivationDerivative() const; //getter of derivative of Activation  fucntion

		void SetError(const double&); //setter of error;
		double GetError() const { return error; } //getter of error;

		void SetAlpha(const double&); //setter of alpha
		double GetAlpha() const { return alpha; }//getter fo alpha

		void SetNeuronType(const NeuronType&); //set different type of neuron;

		void operator=(const Neuron&);
	private://private functions

	public://public variables

	private://private variables
		NeuronType neuronType; //define type of neuron
		double alpha; //coefficient for Activation function
		double state; //state of neuron
		double error; //error in back-propogation algorithm
	};


}
#endif /*_NEURON_*/