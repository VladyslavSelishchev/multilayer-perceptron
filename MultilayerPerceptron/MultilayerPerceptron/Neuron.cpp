#include "Neuron.h"
#include <math.h>

namespace NeuralNetworkNS
{

	Neuron::Neuron() :
		state(0), error(0), alpha(1),
		neuronType(NeuronType::Hidden)
	{
	}

	Neuron::Neuron(NeuronType type, const double& alph = 1) :
		neuronType(type), alpha(alph), state(0), error(0)
	{
	}


	Neuron::~Neuron()
	{
	}

	void Neuron::SetState(const double& st)
	{
		state = st;
	}

	double Neuron::GetActivation() const
	{
		if (neuronType == NeuronType::Dummy)
			return 1;
		else if (neuronType == NeuronType::Input)
			return state;
		else
			return (1 - exp(-alpha*state)) / (1 + exp(-alpha*state));
	}

	double Neuron::GetActivationDerivative() const
	{
		return 2 * alpha*exp(-alpha*state) / (pow(1 + exp(-alpha*state), 2));
	}

	void Neuron::SetError(const double& err)
	{
		error = err;
	}

	void Neuron::SetAlpha(const double& alph)
	{
		alpha = alph;
	}

	void Neuron::SetNeuronType(const NeuronType& type)
	{
		neuronType = type;
		if (neuronType == NeuronType::Dummy)
			state = 1;
	}

	void Neuron::operator=(const Neuron& neuron)
	{
		neuronType = neuron.neuronType;
		state = neuron.state;
		error = neuron.error;
		alpha = neuron.alpha;
	}

}