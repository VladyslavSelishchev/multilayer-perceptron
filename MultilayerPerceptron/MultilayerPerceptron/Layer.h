#ifndef _LAYER_
#define _LAYER_

#include "Neuron.h"
#include <vector>

namespace NeuralNetworkNS
{
	class Layer
	{
		typedef NeuronType LayerType;
	public: //functions
		Layer() :Layer(5, LayerType::Hidden) { /*layerType = LayerType::Hidden;*/ } //defined through another constructor
		Layer(const unsigned int&, const LayerType&);
		~Layer();


		void SetNeuronsNumber(const unsigned int&); //change all configuration of layer(neurons array etc.)
		unsigned int GetNeuronsNumber() const { return neuronsNumber; }

		void SetLayerType(const LayerType&); //set type of neurons in layer
		LayerType GetLayerType() const { return layerType; }

		void SetWeights(const std::vector<std::vector<double>>&); //set predefined matrix of weights
		void SetWeights(const unsigned int&); //randomize matrix of weights (parameter - Number of weight on previous layer) 

		unsigned int GetNeuronsOnPreviuosLayer() const { return neuronsOnPrevLayer; }

		void ChangeOldWeights(); //set oldWeights==weights after Back Propagation

		void operator=(const Layer&);
	private: //functions

	public: //variables
		std::vector<Neuron> neurons; //array of neurons in layer

		std::vector<std::vector<double>> weights;//matrix of connections with previous layer;
		std::vector<std::vector<double>> oldWeights; //matrix of previous generation of weights;

	private: //variables
		unsigned int neuronsNumber;
		LayerType layerType; //type of neurons in layer
		unsigned int neuronsOnPrevLayer; //Number of neurons on previous layer(with Dummy)
	};

}
#endif /*_LAYER_*/