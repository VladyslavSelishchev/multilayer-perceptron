#include "NetworkTrainer.h"



NetworkTrainer::NetworkTrainer(NeuralNetwork& neuralNetwork)
{
	nw = std::unique_ptr<NeuralNetwork>(&neuralNetwork);
}


NetworkTrainer::~NetworkTrainer()
{
}



void NetworkTrainer::ForwardPropagation()
{
	nw->SetInput(sample.inputValues);

	for (size_t i = 1; i < nw->GetLayersNumber(); i++) //layers
	{
		Layer& layer = nw->GetLayer(i);
		for (size_t j = 0; j < layer.GetNeuronsNumber(); j++)//neurons on layer
		{
			double state = 0;
			size_t neuronsOnPrevLayer = nw->GetLayer(i - 1).GetNeuronsNumber() + 1;//with Dummy
			for (size_t k = 0; k < neuronsOnPrevLayer; k++)//neurons on previous layer with Dummy
			{
				state += layer.weights[j][k] * nw->GetLayer(i - 1).neurons[k].GetActivation();
			}

			layer.neurons[j].SetState(state);
		}
	}
}

void NetworkTrainer::BackPropagation()
{
	for (size_t i = nw->GetLayersNumber() - 1; i > 0; i--)
	{
		Layer& layer = nw->GetLayer(i);
		if (i == nw->GetLayersNumber() - 1) //for output layer we have another calculations of error
		{
			for (size_t j = 0; j < layer.GetNeuronsNumber(); j++)
			{
				//calculate error of output neuron
				layer.neurons[j].SetError((layer.neurons[j].GetActivation() - sample.outputValues[j])*
					layer.neurons[j].GetActivationDerivative());
				//calculate weights
				for (size_t k = 0; k < layer.GetNeuronsOnPreviuosLayer(); k++)
				{
					layer.weights[j][k] = layer.weights[j][k] -
						learningRate*layer.neurons[j].GetError()*nw->GetLayer(i - 1).neurons[k].GetActivation();
				}
			}
		}
		else
		{
			Layer& nextLayer = nw->GetLayer(i + 1);
			Layer& prevLayer = nw->GetLayer(i - 1);
			for (size_t j = 0; j < layer.GetNeuronsNumber(); j++)
			{
				//calculate error of neurons
				double error = 0;
				for (size_t k = 0; k < nextLayer.GetNeuronsNumber(); k++)
				{
					error += nextLayer.neurons[k].GetError()*nextLayer.oldWeights[k][j];
				}
				layer.neurons[j].SetError(error*layer.neurons[j].GetActivationDerivative());

				//calculate weights
				for (size_t k = 0; k < prevLayer.GetNeuronsNumber(); k++)
				{
					layer.weights[j][k] = layer.weights[j][k] -
						learningRate*layer.neurons[j].GetError()*prevLayer.neurons[k].GetActivation();
				}
			}
		}
	}
}

void NetworkTrainer::SetSample(const Sample& smpl)
{
	sample = smpl;
}