#ifndef _NEURALNETWORK_
#define _NEURALNETWORK_

#include "Layer.h"
#include <assert.h>

namespace NeuralNetworkNS
{

	class NeuralNetwork
	{
	public: //functions
		NeuralNetwork();
		NeuralNetwork(const unsigned int&);
		NeuralNetwork(const NeuralNetwork&);
		~NeuralNetwork();


		void SetLayersNumber(const unsigned int&);
		unsigned int GetLayersNumber() const { return layersNumber; }

		Layer& GetLayer(const unsigned int&); //return reference to one of the layers of network;
		void RandomizeAllWeights(); //set values of weights on layers in a range [-0.5;0.5];
		void FillWeightsOnLayer(const std::vector<std::vector<double>>&, const unsigned int&);

		void ChangeAllOldWeights();

		void SetInput(const std::vector<double>&);//Set input vector of network
	private: //functions


	public: //variables


	private: //variables
		std::vector<Layer> layers;
		unsigned int layersNumber;
	};

}
#endif /*_NEURALNETWORK_*/