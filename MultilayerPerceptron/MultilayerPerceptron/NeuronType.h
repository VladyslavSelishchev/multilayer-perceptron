#pragma once

namespace NeuralNetworkNS
{

	///Types of Neurons. It uses for giving different values of Activation function
	enum NeuronType
	{
		Input = 0,
		Hidden = 1,
		Dummy = 2,
		Output = 3
	};

}